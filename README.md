# PadiAI

## What is PadiAI ##
PadiAI is an application for using mobile to detect rice blast on vegetation by using a custom trained model created by ITXOTIC Engineering. This application used Tensorflow plug-in technology to enable machine to identify rice blast.

## System Requirement ##
- Mobile phone with build-in camera.
- CPU : Quad-core 1.2 GHz (Minimum)
- RAM : Mimumum 2GB RAM (Recommanded 6GB RAM)
- Minimum 200 MB free storage (Internal Storage Recommanded).
- Operating system must be Android 6.0 (Marshmallow) or above.

## How To Install ##
**Step 1** <br/>
Enable "Unknown sources" in Setting -> Security -> switch "Unknown Sources" to "On".

<img src="/Screenshot/Screenshot_20200219-191727.jpg" width="30%" height="30%"><img src="/Screenshot/Screenshot_20200219-191734.jpg" width="30%" height="30%"><br/>

*Difference mobile model or difference version of Android might not be similar.*

**Step 2** <br/>
Download the latest PadiAI APK file. <br/>

**Step 3** <br/>
Open the downloaded APK in step 2 using any File Manager application or just click from the download list. <br/>

**Step 4** <br/>
Once the APK file open, a permission message will pop-out as below. Click "Next" for proceed to installation process. <br/>

<img src="/Screenshot/Screenshot_20200220-112528.jpg" width="30%" height="30%"><img src="/Screenshot/Screenshot_20200220-112532.jpg" width="30%" height="30%"><br/>

**Step 5** <br/>
After installation completed, a similar windows will pop-out and the application is ready to launch by click "Open" button. <br/>
<img src="/Screenshot/Screenshot_20200220-112624.jpg" width="30%" height="30%"><br/>

*Once the application launch, the detection will automatic start.